#include "AnalysisAdaptor.h"

#include "vtkObjectFactory.h"

namespace sensei
{

//----------------------------------------------------------------------------
AnalysisAdaptor::AnalysisAdaptor()
{
}

//----------------------------------------------------------------------------
AnalysisAdaptor::~AnalysisAdaptor()
{
}

//----------------------------------------------------------------------------
void AnalysisAdaptor::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

} // end of namespace sensei
